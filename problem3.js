module.exports = function sortCarModelsAlphabetically(inventory) {
  const sortedCarModels = [];
  for (let i = 0; i < inventory.length; i++) {
    sortedCarModels.push(inventory[i].car_model);
  }
  // using inbuilt sort method
  sortedCarModels.sort();

  return sortedCarModels;
};
