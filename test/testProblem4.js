const inventory = require("../inventory");
const findCarYears = require("../problem4");

const carYearsArray = findCarYears(inventory);
console.log(carYearsArray);

// This output is required in next problem
module.exports = carYearsArray;
