const inventory = require("../inventory");
const getLastcar = require("../problem2");

const lastCar = getLastcar(inventory);
console.log(`Last car is a ${lastCar.car_make} ${lastCar.car_model}`);
