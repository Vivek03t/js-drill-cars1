module.exports = function countCarsOlderThanYear(carYearsArray, year) {
  let carsBeforeYear = [];
  for (let i = 0; i < carYearsArray.length; i++) {
    if (carYearsArray[i] < year) {
      carsBeforeYear.push(carYearsArray[i]);
    }
  }
  return carsBeforeYear;
};
